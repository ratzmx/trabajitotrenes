package trenecitos2;

/**
 *
 * @author ratzmx
 */
public class Locomotora extends Carro {
    private final int capacidad;
    public Locomotora(int capacidad,int peso) {
        super(peso);
        this.capacidad=capacidad;
    }

    public int getCapacidad() {
        return capacidad;
    } 
}
