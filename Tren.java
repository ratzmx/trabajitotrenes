package trenecitos2;

/**
 *
 * @author ratzmx
 */

public class Tren {
        private Nodo izq,der;
        private final String nombre;
        private int carros,locomotoras;
        
    public Tren(String nombre,int capacidad , int peso) 
    {
        this.nombre=nombre;
        this.izq = null;
        this.der = null;
        carros=0;
        locomotoras=0;
        this.añadir(capacidad , peso,"p");
    }

    public int getCarros()
    {
        return carros;
    }

    public int getLocomotoras() 
    {
        return locomotoras;
    }

    public Nodo getIzq() 
    {
        return izq;
    }

    public Nodo getDer()
    {
        return der;
    }

    public String getNombre() 
    {
        return nombre;
    }
    
    public boolean esVacio()
    {
        return this.izq==null;
    }
    
    public void añadir(int capacidad , int peso , String posicion)
    {
        Nodo n1 = new Nodo(capacidad , peso, "locomotora");
         if (posicion.equalsIgnoreCase("p"))
         {
            this.agregarP(n1);
        }
         else
         {
            this.agregarF(n1);
        }
         this.locomotoras+=1;

    }
    
    public void añadir(int peso)
    {
        Nodo n1 = new Nodo(peso, "carro");
        this.agregarF(n1);
        this.carros+=1;
         
    }
        
    private void agregarP(Nodo n1)
    {
        if (this.izq==null) 
        {
            this.izq=n1;
            this.der=n1;
        }
        else 
        {
        n1.setSig(this.izq);
        this.izq.setAnt(n1);
        this.izq=n1;
        }
    }
    
    private void agregarF(Nodo n1)
    {
        if (this.izq==null)
        {
            this.izq=n1;
            this.der=n1;
        }
        else
        {
        n1.setAnt(this.der);
        this.der.setSig(n1);
        this.der=n1;
        }
    }
    
    public void eliminar(String posicion)
    {
        if (posicion.equalsIgnoreCase("p")) {
            this.eliminarP();
        }
        else{
            this.eliminarF();
        }
    }
    
    private void eliminarP()
    {
        if (!this.esVacio())
        {
            if (this.izq.getSig().getElemento().getTipo().equalsIgnoreCase("locomotora")) 
            {
                this.izq=this.izq.getSig();
                this.izq.setAnt(null);
                this.locomotoras-=1;
            }
            else
            {
                System.out.println("No se puede ya que el siguiente es de tipo " + this.izq.getSig().getElemento().getTipo());
            }
        }
    }
    
    private void eliminarF()
    {
         if (!this.esVacio())
        {
            if (this.der.getAnt()!=null) {
                this.der=this.der.getAnt();
                this.der.setSig(null);
                if (this.izq.getElemento().getTipo().equalsIgnoreCase("locomotora")) 
                {
                    this.locomotoras-=1;
                }
                else
                {
                    this.carros-=1;
                }
            }
            else
            {
                System.out.println("No se puede dejar el tren vacio");
            }
        }
    }

    public void invertir()
    {   
        if (this.der.getElemento().getTipo().equalsIgnoreCase("locomotora")) 
        {
            Nodo aux=this.izq,nizq=null,nder=null,nuevo;
            while(aux!=null)
            {
                if (aux.getElemento().getTipo().equalsIgnoreCase("locomotora")) {
                   nuevo=new Nodo(aux.getElemento().getCapacidad(),aux.getElemento().getPeso(),"locomotora");
                }
                else
                {
                   nuevo = new Nodo(aux.getElemento().getPeso(),"carro");
                }
                if (nizq!=null) {
                   nuevo.setSig(nizq);
                   nizq.setAnt(nuevo);
                   nizq=nuevo;
                }
                else
                {
                    nizq=nuevo;
                    nder=nuevo;
                }
            aux=aux.getSig();
            }
            this.izq=nizq;
            this.der=nder;
        }
        else
        {
            System.out.println("No se puede invertir ya que no hay locomotora al final del carro");
        }
    }
    
    public String imprimir()
    {
        String res="";
        Nodo aux =this.izq;
        res+=" [ ";
        while(aux!=null)
        {
            if(aux.getElemento().getTipo().equalsIgnoreCase("locomotora")){
                res+="[ " + aux.getElemento().getTipo()+" , " + aux.getElemento().getCapacidad()+" , " + aux.getElemento().getPeso()+ " ] ";
            }
            else
            {
                res+="[ " + aux.getElemento().getTipo()+"  ,  " + aux.getElemento().getPeso()+ " ] ";
            }
            if (aux.getSig()!=null) {
                res+="- ";
            }
            aux=aux.getSig();
        }
        res+=" ] ";
        return res;
    }
    
    public void concatenarTrenes(Tren n1)
    {
        Nodo aux = n1.izq;
        while(aux!=null)
        {
            this.agregarF(aux);
            aux=aux.getSig();
        }
    }
}
