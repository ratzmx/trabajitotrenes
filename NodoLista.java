package trenecitos2;

/**
 *
 * @author ratzmx
 */
public class NodoLista {
    private final Tren  elemento;
    private NodoLista sig,ant,rec;
    
    public NodoLista(Tren elemento)
    {
        this.sig=null;
        this.ant=null;
        this.rec=null;
        this.elemento=elemento;
    }

    public void setSig(NodoLista sig) {
        this.sig = sig;
    }

    public void setAnt(NodoLista ant) {
        this.ant = ant;
    }

    public Tren getElemento() {
        return elemento;
    }

    public NodoLista getSig() {
        return sig;
    }

    public NodoLista getAnt() {
        return ant;
    }


}

