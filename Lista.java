package trenecitos2;

/**
 *
 * @author ratzmx
 */

public class Lista {
    private NodoLista izq,der;
    private int trenes;
    
    public Lista() 
    {
        this.izq = null;
        this.der = null;
    }
    
    private boolean esVacia()
    {
        return this.izq==null;
    }
    
    
    private void popizq()
    {
        if(this.izq==this.der)
        {
            this.der=null;
            this.izq=null;
        }
        else
        {
            this.izq=izq.getSig();
            this.izq.setAnt(null);
        }
    }
    
    private NodoLista buscarTren(String nombreTren)
    {
        NodoLista n1 = this.izq;
        if (this.esta(nombreTren)) 
        {
            while(n1!=null)
            {
                if (n1.getElemento().getNombre().equalsIgnoreCase(nombreTren)) {
                    return n1;
                }
                n1=n1.getSig();
            }
        }
        return null;
    }
    
    private void  agregarizq (NodoLista n1)
    {
        if (this.esVacia()) {
            this.izq=n1;
            this.der=n1;
        }
        else
        {
            n1.setSig(this.izq);
            this.izq.setAnt(n1);
            this.izq=n1;
        }
    }

    private void  agregarder (NodoLista n1)
    {
        if (this.esVacia()) {
            this.izq=n1;
            this.der=n1;
        }
        else
        {
            n1.setAnt(this.der);
            this.der.setSig(n1);
            this.der=n1;
        }
    }
             
    private void Concatenar(Lista n2)
    {
        NodoLista n1 = n2.izq;
        while(n1!=null)
        {
            this.agregarizq(n1);
            System.out.println(n1.getElemento().getNombre());
            n1=n1.getSig();
            System.out.println(n1.getElemento().getNombre());
        }
    }
    
    private Lista copiarlista()
    {
        Lista l1 = new Lista();
        NodoLista aux=this.izq;
        while(aux!=null){
            l1.agregarder(aux);
            aux=aux.getSig();
        }
        return l1;
    }
    
    private boolean esta(String nombreTren)
    {
        NodoLista n1 = this.izq;
        while(n1!=null)
        {
            if(n1.getElemento().getNombre().equalsIgnoreCase(nombreTren))
            {
                return true;
            }
            n1=n1.getSig();
        }
        return false;
    }
    
    //************************************************************************************************************
    //*                                 Comienzo clases publicas                                                                         *
    //************************************************************************************************************

    public void CrearTren(String nombreTren,int CapacidadLoc,int TonelajeLoc)
    {
        Tren n1 = new Tren(nombreTren,CapacidadLoc,TonelajeLoc);
        NodoLista n2 = new NodoLista(n1);
        if (this.esVacia()) {
            this.izq=n2;
            this.der=n2;
        }
        else
        {
            n2.setSig(this.izq);
            this.izq.setAnt(n2);
            this.izq=n2;
        }
        this.trenes+=1;
        
    }
        
    public void añadirCarro (String nombreTren, int peso )
    {
        if (this.esta(nombreTren)) 
        {
            if (this.der.getElemento().getNombre().equalsIgnoreCase(nombreTren)) 
            {
                this.der.getElemento().añadir( peso);
            }
            else
            {
            NodoLista res = this.buscarTren(nombreTren);
            res.getElemento().añadir(peso);
            }
        }
    }
    
    public void añadirLocomotora(String nombreTren,int capacidad, int peso , String pos)
    {
        if (this.esta(nombreTren))
        {
            if (this.der.getElemento().getNombre().equalsIgnoreCase(nombreTren)) 
            {
                this.der.getElemento().añadir(capacidad, peso, pos);
            }
            else
            {
                NodoLista res = this.buscarTren(nombreTren);
                res.getElemento().añadir(capacidad, peso, pos);
            }
        }
    }
    
    public void imprimir()
    {
        Lista n1 = this.copiarlista();
        while(!n1.esVacia())
        {
            System.out.println(n1.izq.getElemento().imprimir());
            if (n1.izq.getSig()!=null)
            {
                n1.popizq();
            }
            else
            {
                break;
            }
        }
    }
    
    public void eliminarTren(String nombreTren)
    {
        NodoLista n1 = this.buscarTren(nombreTren);
        if (n1!=null)
        {
            if (n1.getAnt()==null&&this.trenes>1) 
            {
                this.izq=n1.getSig();
                this.izq.setAnt(null);
            }
            if (n1.getSig()==null&&this.trenes>1) 
            {
                this.der=n1.getAnt();
                this.der.setSig(null);
            }
            if(n1.getSig()!=null && n1.getAnt()!=null)
            {
                n1.getAnt().setSig(n1.getSig());
                n1.getSig().setAnt(n1.getAnt());
            }
            
            this.trenes-=1;
        }
    }

    public void eliminar(String nombreTren,String pos)
    {
        NodoLista n1 = this.buscarTren(nombreTren);
        n1.getElemento().eliminar(pos);
    }

    /*
    public void invertir(String nombreTren)
    {
        Lista nueva = this.buscarTren(nombreTren);
        this.izq.getElemento().invertir();
        this.Concatenar(nueva);
    }
    */

    public void listar()
    {
        Lista n1 = this.copiarlista();
        System.out.println("Cantidad de trenes: "+this.trenes);
        while(!n1.esVacia())
        {
            System.out.println("Nombre tren: " + n1.izq.getElemento().getNombre()+" , Numero de Locomotoras: " + n1.izq.getElemento().getLocomotoras() + " , Numero de Carros: "+ n1.izq.getElemento().getCarros());
            n1.popizq();
        }
    }
    
    public void concatenarListas(String nombre1,String nombre2)
    {
        NodoLista n1 = this.buscarTren(nombre1);
        NodoLista n2 = this.buscarTren(nombre2);
        n1.getElemento().concatenarTrenes(n2.getElemento());
        this.eliminarTren(nombre2);
    }
}
