package trenecitos2;

/**
 *
 * @author ratzmx
 */
public class Partes_Tren {
    private final Carro c1;
    private final Locomotora l1;
    private final String tipo;

    public Partes_Tren( int peso,int capacidad) {
        this.c1 = null;
        this.l1 = new Locomotora(peso,capacidad);
        this.tipo = "locomotora";
    }
        public Partes_Tren(int peso) {
        this.c1 = new Carro(peso);
        this.l1 =null;
        this.tipo = "carro";
    }
    public String getTipo(){
        return this.tipo;
    }
    
    public int getPeso(){
        if (this.tipo.equalsIgnoreCase("carro")) {
            return this.c1.getpeso();
        }
        else{
            return this.l1.getpeso();
        }
    }
    
    public int getCapacidad(){
        if (this.tipo.equalsIgnoreCase("locomotora")) {
            return this.l1.getCapacidad();
        }
        else{
            System.out.println("Un carro no tiene capacidad de tiro");
            return 0;
        }
    }
}
