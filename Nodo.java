package trenecitos2;

/**
 *
 * @author ratzmx
 */
public class Nodo {
    private final Partes_Tren  elemento;
    private Nodo sig,ant;

    public Nodo(int peso,int capacidad,String tipo) {
        this.elemento = new Partes_Tren(peso , capacidad) ;
        this.sig = null;
        this.ant = null;
    }
    
    public Nodo(int peso,String tipo) {
        this.elemento = new Partes_Tren(peso) ;
        this.sig = null;
        this.ant = null;
    }

    public void setSig(Nodo sig) {
        this.sig = sig;
    }

    public void setAnt(Nodo ant) {
        this.ant = ant;
    }

    public Partes_Tren getElemento() {
        return elemento;
    }

    public Nodo getSig() {
        return sig;
    }

    public Nodo getAnt() {
        return ant;
    }
}

